function setErrorMessage(elementId, visible) {
	
	const selector = '#' + elementId + ' ~ .error-msg';
	let messageNode = document.querySelector(selector);	
	let inputElement = document.querySelector('input#' + elementId);
	
	
	if (visible) {
		messageNode.classList.remove('oculto');
		inputElement.setCustomValidity(messageNode.innerText);
	} else {
		messageNode.classList.add('oculto');
		inputElement.setCustomValidity('');
	}
}

function showErrorFor(elementId) {
	setErrorMessage(elementId, true);
}

function hideErrorFor(elementId) {
	setErrorMessage(elementId, false);
}

function checkName() {
	let txtNome = document.querySelector('#nome');
	let valid = txtNome.value.trim().length > 0;
	if (!valid) {
		showErrorFor(txtNome.id);
		txtNome.focus();
	} else {
		hideErrorFor(txtNome.id);
	}
}

function checkUser(inputElement) {
	let xhr = new XMLHttpRequest();
	xhr.onload = function() {
		if (xhr.responseText == 'true') {
			showErrorFor(inputElement.id);
			inputElement.focus();
		} else {
			hideErrorFor(inputElement.id);
		}
	};
	
	let username = inputElement.value;
	let url = 'cadastrar?cmd=exists&username=' + username ;
	xhr.open('GET', url);
	xhr.send();
}


function checkPassword() {
	let txtPassword = document.querySelector('#password');
	let txtPasswordConfirm = document.querySelector('#password-confirm');
	
	if (txtPassword.value !== txtPasswordConfirm.value) {
		showErrorFor(txtPasswordConfirm.id);
	} else {
		hideErrorFor(txtPasswordConfirm.id);
	}
}

