package edu.ifsp.lojinha.persistencia;

import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.h2.tools.RunScript;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;



@WebListener
public class H2Listener implements ServletContextListener {
	private static final boolean DEBUG = true;
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("[H2Listener] Preparando banco de dados de testes...");
		final ServletContext servletContext = sce.getServletContext();

			
		try (Connection conn = DatabaseConnector.getConnection()) {

			
			try (InputStreamReader reader = new InputStreamReader(
					servletContext.getResourceAsStream("WEB-INF/resources/schema.sql"))) {
				RunScript.execute(conn, reader);
			}
			
			
			try (InputStreamReader reader = new InputStreamReader(
					servletContext.getResourceAsStream("WEB-INF/resources/data.sql"))) {
				RunScript.execute(conn, reader);
			}
			
			
			if (DEBUG) {
				
				try (Statement stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT * FROM usuario;")) {
					while (rs.next()) {
						System.out.printf("%d %s\n", rs.getLong("id"), rs.getString("username"));
					}
					rs.close();
					stmt.close();
				}
			}
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		System.out.println("[H2Listener] Banco de dados pronto.");
	}
}
