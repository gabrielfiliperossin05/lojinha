package edu.ifsp.lojinha.persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnector {
	static {
		/* carregando driver JDBC do banco de dados H2 */
		try {
			Class.forName("org.h2.Driver");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static Connection getConnection() throws SQLException {
		/*
		 * O par�metro DB_CLOSE_DELAY=-1 faz com que o banco de dados permane�a 
		 * em mem�ria enquanto a VM estiver em execu��o. Sem esse par�metro, o
		 * banco de dados � descartado da mem�ria quando a �ltima conex�o for
		 * fechada.
		 * 
		 * Detalhes na documenta��o:
		 * http://h2database.com/html/features.html#in_memory_databases 
		 */
		Connection conn = DriverManager.getConnection("jdbc:h2:mem:lojinhadb;DB_CLOSE_DELAY=-1", "sa", "");
		return conn;
	}
	
}
